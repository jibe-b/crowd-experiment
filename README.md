# Crowd experiment

Présentation de l'activation d'une dynamique autour des sciences ouvertes, via les réseaux sociaux. Toutes les étapes sont décrites dans un notebook au fur et à mesure, de manière à rendre ce process transparent et auditable, ainsi que permettre aux personnes faisant partie de cette expérience d'être informées 
de ce qui a été mis en place.


Accéder au notebook : [index.ipynb](index.ipynb)
